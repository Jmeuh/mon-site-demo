window.onload = function() {
    $('#home').appendTo('#principal')
    $('#home').addClass('containerOn')
}

let btns = ['#btnHome', '#cvbtn', '#progbtn']
let containeds = ['#home', '#containerCV', '#containerFormation']


function objectOff() {
    for(j = 0; j < btns.length; j++) {
        let btnOff = btns[j]
        let containedOff = containeds[j]
        $(btnOff).removeClass('btnOn')
        $(containedOff).removeClass('containerOn')
    }
}

for(i = 0; i < btns.length; i++) {
    let btn = btns[i]
    let contained = containeds[i]

        // $(pdf).slideUp(0)

    $(btn).click(function() {
        objectOff()
        $(contained).toggleClass('containerOn')
        $(this).toggleClass('btnOn')
        $('#header').slideUp(1000)
        if(btn == btns[0]) {
            $('#header').slideDown(1000)
            $(this).removeClass('btnOn')
        }
    })
}

$('#progAccess').click(function() {
    objectOff()
    $('#containerFormation').toggleClass('containerOn')
    $('#progbtn').toggleClass('btnOn')
    $('#header').slideUp(1000)
})